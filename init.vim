lua require('plugins')

set updatetime=250
set timeoutlen=1000

set termguicolors

set t_ZH=[3m
set t_ZR=[23m

let g:python3_host_prog='/usr/bin/python3.9'
let g:coq_settings={ 'auto_start': v:true, 'keymap.recommended': v:true, 'keymap.eval_snips': '<leader>j' }

let g:dashboard_default_executive = 'telescope'

let g:vimsyn_embed='lPr'
let g:vimsyn_folding='afp'

let g:vimsyn_minlines=1000
let g:vimsyn_maxlines=2000

let g:loaded_matchit=1

set showmatch
set cursorline
set culopt=screenline
set incsearch
set hlsearch
set laststatus=2
set noconfirm
set noshowmode
set nowrap
set numberwidth=3
set signcolumn=yes:2
set number
set relativenumber
set ruler
set scrolloff=5
set shortmess=filnxtToOFc
set sidescrolloff=0
set textwidth=120

set completeopt=menuone,noselect,noinsert

set pumblend=15
set pumheight=5
set pumwidth=12

set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab
set autoindent 
set smartindent

set hidden

set conceallevel=0
set concealcursor=nvi

set foldenable
set foldmethod=indent
set autowrite
set breakindent

set termguicolors
lua << EOF
require("bufferline").setup{}
EOF

lua << EOF
require("revj").setup{}
EOF

lua << EOF

local sumneko_root_path = vim.fn.stdpath('data')..'/lua-language-server/'
local sumneko_binary = sumneko_root_path..'/bin/lua-language-server'
local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

require('nvim-web-devicons').setup()
require('colorizer').setup()
-- Lua
require("lsp-colors").setup({
  Error = "#db4b4b",
  Warning = "#e0af68",
  Information = "#0db9d7",
  Hint = "#10B981"
})
require("illuminate")
require('zen-mode').setup()
require('twilight').setup()
require('todo-comments').setup()

require('nvim-autopairs').setup {
    disable_filetype = { 'TelescopePrompt' },
    ignored_next_char = string.gsub([[ [%w%%%'%[%"%.] ]],"%s+", ""),
    enable_moveright = true,
    enable_afterquote = true,  -- add bracket pairs after quote
    enable_check_bracket_line = true,  --- check bracket in same line
    check_ts = true,
    map_bs = true,  -- map the <BS> key
}

require('nvim-tree').setup({
    disable_netrw = true,
    hijack_netrw = true,
    open_on_setup = false,
    ignore_ft_on_setup = {},
    auto_close = false,
    open_on_false = false,
    hijack_cursor = true,
    update_cwd = true,
    update_focused_file = {
        enable = true,
        ignore_list = {},
    },
})

require('treesitter-context').setup {
    enable = true,
    throttle = false
}

-- TODO: handle this vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.handlers.signature_help
vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.handlers.signature_help
require'lsp_signature'.setup {
    bind = true, -- This is mandatory, otherwise border config won't get registered.
    handler_opts = {
        border = 'single'
    },
}
require('lspkind').init({
-- enables text annotations
--
-- default: true
with_text = true,

-- default symbol map
-- can be either 'default' (requires nerd-fonts font) or
-- 'codicons' for codicon preset (requires vscode-codicons font)
--
-- default: 'default'
preset = 'codicons',

-- override preset symbols
--
-- default: {}
symbol_map = {
    Text = "",
    Method = "",
    Function = "",
    Constructor = "",
    Field = "ﰠ",
    Variable = "",
    Class = "ﴯ",
    Interface = "",
    Module = "",
    Property = "ﰠ",
    Unit = "塞",
    Value = "",
    Enum = "",
    Keyword = "",
    Snippet = "",
    Color = "",
    File = "",
    Reference = "",
    Folder = "",
    EnumMember = "",
    Constant = "",
    Struct = "פּ",
    Event = "",
    Operator = "",
    TypeParameter = ""
    },
})
require('nvim-treesitter.configs').setup {
    highlight = {
        enable = true
    },
    indent = {
        enable = false,
    },
    rainbow = {
        enable = true,
        -- disable = {"cpp"}
        extended_mode = true,
        max_file_lines = nil,
        -- colors = {},  # table of hex strings
        -- termcolors = {},  # table of color name strings
    },
}

local protocol = require'vim.lsp.protocol'

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer

local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    require("lsp_signature").on_attach(client)

    local opts = { noremap=true, silent=true }
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('i', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
    buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
    buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
    buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
    buf_set_keymap("n", '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

    -- formatting
    protocol.CompletionItemKind = {
        '', -- Text
        '', -- Method
        '', -- Function
        '', -- Constructor
        '', -- Field
        '', -- Variable
        '', -- Class
        'ﰮ', -- Interface
        '', -- Module
        '', -- Property
        '', -- Unit
        '', -- Enum
        '', -- Keyword
        '﬌', -- Snippet
        '', -- Color
        '', -- File
        '', -- Reference
        '', -- Folder
        '', -- EnumMember
        '', -- Constant
        '', -- Struct
        '', -- Event
        'ﬦ', -- Operator
        '', -- TypeParameter
    }
    end

    local border_style = 'single'

    local coq = require("coq")
    local nvim_lsp = require('lspconfig')

    nvim_lsp.vimls.setup(coq.lsp_ensure_capabilities({
    on_attach = on_attach,
    flags = {
        debounce_text_changes = 150,
    },
    filetypes = { 'vim' },
    options = {
        ['textDocument/publishDiagnostics'] = {
            virtual_text = false
        },
        ['textDocument/hover'] = {
            border = border_style
        },
        ['textDocument/signatureHelp'] = {
            border = border_style
        },
    }
    }))

    nvim_lsp.yamlls.setup(coq.lsp_ensure_capabilities())
    nvim_lsp.bashls.setup(coq.lsp_ensure_capabilities())
    nvim_lsp.jsonls.setup(coq.lsp_ensure_capabilities())
    nvim_lsp.ccls.setup(coq.lsp_ensure_capabilities())
    nvim_lsp.sqlls.setup(coq.lsp_ensure_capabilities())
    nvim_lsp.pylsp.setup(coq.lsp_ensure_capabilities({
        on_attach = on_attach,
        cmd = { 'pylsp' },
        flags = {
            debounce_text_changes = 150,
            },
        filetypes = { 'python' },
        options = {
            ['textDocument/publishDiagnostics'] = {
                virtual_text = false
                },
            ['textDocument/hover'] = {
                border = border_style
                },
            ['textDocument/signatureHelp'] = {
                border = border_style
            },
        },
    }))



-- Section: sumneko lua language server

nvim_lsp.sumneko_lua.setup(coq.lsp_ensure_capabilities({
on_attach = on_attach,
cmd = { sumneko_binary, '-E', sumneko_root_path .. '/main.lua' },
settings = {
    Lua = {
        runtime = {
            version = 'LuaJIT',
            path = runtime_path,
            },
        diagnostics = {
            globals = {'vim'},
            },
        workspace = {
            library = {
                [vim.fn.expand('$VIMRUNTIME/lua')] = true,
                [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
                },
            },
        telemetry = {
        enable = false,
        }
    }
}
    -- handlers = {
    --     ['textDocument/publishDiagnostics'] = {
    --         },
    --     ['textDocument/hover'] = {
    --         border = border_style
    --         },
    --     ['textDocument/signatureHelp'] = {
    --         border = border_style
    --         }
    --     }
}))

require'marks'.setup {
    default_mappings = true, 
    builtin_marks = {}, 
    cyclic = true, 
    force_write_shada = false, 
    refresh_interval = 250,
    sign_priorty = { lower=10, upper=15, builtin=8, bookmark=20 },
    bookmark_0 = { 
        sign = "⚑",
        virt_text = "hello world"
        },
    bookmark_1 = { 
        sign = "",
        virt_text = "hello world"
        },
    bookmark_2 = { 
        sign = "",
        virt_text = "added"
        },
    bookmark_3 = { 
        sign = "柳",
        virt_text = "changed"
        },
    bookmark_4 = { 
        sign = "",
        virt_text = "DELETED"
        },
    bookmark_5 = { 
        sign = " ",
        virt_text = "class"
        },
    bookmark_6 = { 
        sign = "",
        virt_text = "function"
        },
    bookmark_7 = { 
        sign = "",
        virt_text = "method"
        },
    bookmark_8 = { 
        sign = "⛶",
        virt_text = "container"
        },
    bookmark_9 = { 
        sign = " ",
        virt_text = "tag"
        },
    }

require("toggleterm").setup({
    -- size can be a number or function which is passed the current terminal
    size=17,
    open_mapping = [[<c-\>]],
    hide_numbers = true, -- hide the number column in toggleterm buffers
    shade_filetypes = {},
    shade_terminals = true,
    shading_factor = 1, -- the degree by which to darken to terminal colour, default: 1 for dark backgrounds, 3 for light
    start_in_insert = true,
    insert_mappings = true, -- whether or not the open mapping applies in insert mode
    persist_size = true,
    direction = 'float', --'horizontal' | 'window' | 'float',
    close_on_exit = true, -- close the terminal window when the process exits
    shell = vim.o.shell, -- change the default shell
    -- This field is only relevant if direction is set to 'float'
    float_opts = {
        -- The border key is *almost* the same as 'nvim_open_win'
        -- see :h nvim_open_win for details on borders however
        -- the 'curved' border is a custom border type
        -- not natively supported but implemented in this plugin.
        border = 'single', -- | 'double' | 'shadow' | 'curved' | ... other options supported by win open
        width = 100,
        height = 100,
        winblend = 3,
        highlights = {
            border = "Normal",
            background = "Normal",
            }
        }
    })

require('telescope').setup({
    defaults = {
        vimgrep_arguments = {
            'rg',
            '--color=never',
            '--no-heading',
            '--with-filename',
            '--line-number',
            '--column',
            '--smart-case'
            },
        prompt_prefix = '> ',
        selection_caret = '> ',
    entry_prefix = '  ',
    initial_mode = 'insert',
    selection_strategy = 'reset',
    sorting_strategy = 'descending',
    layout_strategy = 'horizontal',
    layout_config = {
        horizontal = {
            mirror = false,
            },
        vertical = {
            mirror = false,
            },
        },
    file_sorter =  require'telescope.sorters'.get_fuzzy_file,
    file_ignore_patterns = {},
    generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
    winblend = 1,
    border = {},
    borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰' },
    color_devicons = true,
    use_less = true,
    path_display = {},
    set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
    file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
    grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
    qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,
    -- Developer configurations: Not meant for general override
    buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker,
    }
})


require('gitsigns').setup({
    signs = {
        add          = {hl = 'GitSignsAdd'   , text = ' ', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'   },
        change       = {hl = 'GitSignsChange', text = ' ', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
        delete       = {hl = 'GitSignsDelete', text = ' ', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
        topdelete    = {hl = 'GitSignsDelete', text = ' ', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
        changedelete = {hl = 'GitSignsChange', text = ' ', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
        },
    signcolumn = true,  -- Toggle with `:Gitsigns toggle_signs`
    numhl      = false, -- Toggle with `:Gitsigns toggle_numhl`
    linehl     = false, -- Toggle with `:Gitsigns toggle_linehl`
    word_diff  = false, -- Toggle with `:Gitsigns toggle_word_diff`
    keymaps = {
        -- Default keymap options
        noremap = true,

        ['n ]c'] = { expr = true, "&diff ? ']c' : '<cmd>Gitsigns next_hunk<CR>'"},
        ['n [c'] = { expr = true, "&diff ? '[c' : '<cmd>Gitsigns prev_hunk<CR>'"},

        ['n <leader>hs'] = '<cmd>Gitsigns stage_hunk<CR>',
        ['v <leader>hs'] = ':Gitsigns stage_hunk<CR>',
        ['n <leader>hu'] = '<cmd>Gitsigns undo_stage_hunk<CR>',
        ['n <leader>hr'] = '<cmd>Gitsigns reset_hunk<CR>',
        ['v <leader>hr'] = ':Gitsigns reset_hunk<CR>',
        ['n <leader>hR'] = '<cmd>Gitsisgns reset_buffer<CR>',
        ['n <leader>hp'] = '<cmd>Gitsigns preview_hunk<CR>',
        ['n <leader>hb'] = '<cmd>lua require"gitsigns".blame_line{full=true}<CR>',
        ['n <leader>hS'] = '<cmd>Gitsigns stage_buffer<CR>',
        ['n <leader>hU'] = '<cmd>Gitsigns reset_buffer_index<CR>',

        -- Text objects
        ['o ih'] = ':<C-U>Gitsigns select_hunk<CR>',
        ['x ih'] = ':<C-U>Gitsigns select_hunk<CR>'
    },
    watch_gitdir = {
        interval = 1000,
        follow_files = true,
        },
    attach_to_untracked = true,
    current_line_blame = false, -- Toggle with `:Gitsigns toggle_current_line_blame`
    current_line_blame_opts = {
        virt_text = true,
        virt_text_pos = 'eol', -- 'eol' | 'overlay' | 'right_align'
        delay = 1000,
        },
    current_line_blame_formatter_opts = {
        relative_time = false,
        },
    sign_priority = 6,
    update_debounce = 100,
    status_formatter = nil, -- Use default
    max_file_length = 40000,
    preview_config = {
        -- Options passed to nvim_open_win
        border = 'single',
        style = 'minimal',
        relative = 'cursor',
        row = 0,
        col = 1,
        },
    yadm = {
        enable = false
    }
})

require('numb').setup()

-- require'statusbar'

require('Comment').setup()

-- local saga = require 'lspsaga'
-- saga.init_lsp_saga()

-- NOTE: Colorscheme configuration



-- local onedarkpro = require('onedarkpro')
--
-- onedarkpro.setup({
--   theme = "onedark", -- Override with "onedark" or "onelight". Alternatively, don't specify a value and let `vim.o.background` set the theme
--   -- colors = {
--   -- }, -- Override default colors. Can specify colors for "onelight" or "onedark" themes
--   -- hlgroups = {}, -- Override default highlight groups
--   styles = {
--       strings = "NONE", -- Style that is applied to strings
--       comments = "italic", -- Style that is applied to comments
--       keywords = "NONE", -- Style that is applied to keywords
--       functions = "bold", -- Style that is applied to functions
--       variables = "NONE", -- Style that is applied to variables
--   },
--   options = {
--       bold = true, -- Use the themes opinionated bold styles?
--       italic = true, -- Use the themes opinionated italic styles?
--       underline = true, -- Use the themes opinionated underline styles?
--       undercurl = false, -- Use the themes opinionated undercurl styles?
--       cursorline = true, -- Use cursorline highlighting?
--       transparency = true, -- Use a transparent background?
--   }
-- })
-- onedarkpro.load()

-- require("dressing").setup()

EOF

let g:minimap_auto_start=1
let g:minimap_auto_start_win_enter=1
let g:minimap_width=20


let g:SimpylFold_docstring_preview = 1
let g:SimpylFold_fold_docstring = 1
let g:SimpylFold_fold_import = 1

let g:nvcode_termcolors=256
syntax on
set background=dark
colorscheme onedark

highlight Normal guibg=#2A2A2A
highlight SignColumn guibg=#2A2A2A

" guifg=#9E9E9E 
"hi IlluminatedWord guifg=#9E9E9E guibg=#444444
" hi LineNr cterm=NONE gui=NONE 
"hi CursorLineNr cterm=NONE gui=NONE
hi Comment cterm=italic gui=italic
hi DiffAdd cterm=NONE gui=NONE guifg=#859900 guibg=NONE
hi DiffDelete cterm=NONE gui=NONE guifg=#FF7370 guibg=NONE
hi DiffChange cterm=NONE gui=NONE guifg=#E5C07B guibg=NONE

" SECTION: keymaps

nnoremap <leader>rn :Lspsaga rename
nnoremap <leader>ff <cmd>Telescope fd<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>qf <cmd>Telescope quickfix<cr>
nnoremap <leader>to <cmd>Telescope<cr>
nnoremap <leader>sv :w \| source % \| PackerSync<cr>
nnoremap <leader>ev <cmd>e ~/.config/nvim/init.vim<cr>
nnoremap [h :set hlsearch!<cr>
nnoremap [s :set spell!<cr>
nnoremap [sb :set scrollbind!<cr>
nnoremap <leader>rr <cmd>:w \| !black --target-version py310 % \| clear \| python3 %<cr>
nnoremap <leader>z <cmd>ZenMode<cr>
nnoremap <leader>s <cmd>Twilight<cr>
nnoremap <C-n> <cmd>NvimTreeToggle<CR>
nnoremap <F8> <cmd>Tagbar<cr>
nnoremap <C-h> <cmd>bprev<cr>
nnoremap <C-l> <cmd>bnext<cr>

" SECTION: Tabularize
nnoremap <leader># :Tabularize /#/l2l1<CR>

" SECTION: (macros)
let @o = 'saiw)bi'
let @p = 'saiw)biprint%%l'

" SECTION: akinsho/bufferline
nnoremap <silent>[b :BufferLineCycleNext<CR>
nnoremap <silent>]b :BufferLineCyclePrev<CR>


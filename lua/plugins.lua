-- This file can be loaded by

return require('packer').startup(function(use)
    -- SECTION: Packer plugin manager (manages itself)
    use 'wbthomason/packer.nvim'
    -- SECTION: nvim_lsp and general prerequisites
    use 'nvim-lua/popup.nvim'
    use 'nvim-lua/plenary.nvim'
    -- SECTION: icon support
    use 'ryanoasis/vim-devicons'
    use 'kyazdani42/nvim-web-devicons'
    -- IMPORTANT: nvim-lspconfig (nvim lsp support)
    use 'neovim/nvim-lspconfig'
    -- SECTION: treesitter
    use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
    -- SECTION: treesitter extensions
    use 'nvim-treesitter/nvim-treesitter-textobjects'
    use 'JoosepAlviste/nvim-ts-context-commentstring'
    use 'p00f/nvim-ts-rainbow'

    -- SECTION: autocompletion (using coq)
    use { 'ms-jpq/coq_nvim', branch = 'coq' }
    use { 'ms-jpq/coq.artifacts', branch = 'artifacts' }
    use { 'ms-jpq/coq.thirdparty', branch = '3p' }

    -- SECTION: Search utility
    use 'nvim-telescope/telescope.nvim'
    -- SECTION: git vcs support
    use 'lewis6991/gitsigns.nvim'
    -- SECTION: editor extensions
    use 'andymass/vim-matchup'  -- pair matching extensions
    use 'machakann/vim-highlightedyank'
    use 'machakann/vim-sandwich'  -- surround extension

    use "folke/todo-comments.nvim"  -- code comment documentation extension
    -- use "folke/trouble.nvim"
    -- use "folke/which-key.nvim"
    use "norcalli/nvim-colorizer.lua"
    --    use {
    --	    "ms-jpq/chadtree",
    --	    branch = 'chad',
    --	    run = 'python3 -m chadtree deps'
    --    }
    -- SECTION: manual file tree navigation and editing support
    use 'kyazdani42/nvim-tree.lua'
    -- SECTION: lsp extensions
    use "folke/lsp-colors.nvim"
    use "ray-x/lsp_signature.nvim"
    use "glepnir/lspsaga.nvim"
    use "onsails/lspkind-nvim"

    -- use "ervandew/screen"
    use "tpope/vim-speeddating"
    -- NOTE: Calendar
    use "itchyny/calendar.vim"
    -- NOTE: Markdown
    use "godlygeek/tabular"
    use "vim-pandoc/vim-pandoc"
    use "vim-pandoc/vim-pandoc-syntax"
    use "plasticboy/vim-markdown"
    use { "iamcco/markdown-preview.nvim", run = 'cd app & yarn install', cmd = 'MarkdownPreview' }
    -- NOTE: Marks
    use "chentau/marks.nvim"
    use "nacro90/numb.nvim"
    use "tmhedberg/SimpylFold"
    -- use "hoob3rt/lualine.nvim"
    use ({
        "NTBBloodbath/galaxyline.nvim",
        -- your statusline
        config = function()
            require("galaxyline.themes.eviline")
        end,
        -- some optional icons
        requires = {'kyazdani42/nvim-web-devicons', opt = true}
    })
    use "glepnir/dashboard-nvim"
    use "preservim/tagbar"
    use "habamax/vim-asciidoctor"
    use "olimorris/onedark.nvim"
    use "windwp/nvim-autopairs"
    --use "lukas-reineke/onedark.nvim"
    use "raimon49/requirements.txt.vim"
    --use "tpope/vim-commentary"
    --use "rakr/vim-one"
    --use "tomasiser/vim-code-dark"
    use "romgrk/nvim-treesitter-context"
    use "akinsho/toggleterm.nvim"
    use "folke/twilight.nvim"
    use "folke/zen-mode.nvim"
    -- use {
    --     "SmiteshP/nvim-gps",
    --     requires = "nvim-treesitter/nvim-treesitter"
    -- }
    use "tpope/vim-repeat"
    use "christianchiarulli/nvcode-color-schemes.vim"
    use "RRethy/vim-illuminate"
    --use "Yagua/nebulous.nvim"
    --use "joshdick/onedark.vim"
    -- SECTION: colorschemes
    use "NTBBloodbath/doom-one.nvim"
    use "savq/melange"
    use "morhetz/gruvbox"
    use "ozkanonur/nimda.vim"
    use "sainnhe/gruvbox-material"
    use "sainnhe/sonokai"
    use "bluz71/vim-moonfly-colors"
    use "artanikin/vim-synthwave84"
    use "lifepillar/vim-solarized8"
    use "folke/tokyonight.nvim"

    -- use "romgrk/barbar.nvim"
    use {'akinsho/bufferline.nvim', requires = 'kyazdani42/nvim-web-devicons'}

    use "luukvbaal/stabilize.nvim"

    use "numToStr/Comment.nvim"
    use "sbulav/jump-ray.nvim"

    -- use "lukas-reineke/indent-blankline.nvim"
    -- use "yorickpeterse/vim-paper"
    -- use "NLKNguyen/papercolor-theme"
    -- use "ajgrf/parchment"
    use "Mangeshrex/uwu.vim"
    use 'Mofiqul/vscode.nvim'
    --
    use "xolox/vim-misc"
    use "xolox/vim-notes"

    use "mechatroner/rainbow_csv"

    --use "chrisbra/csv.vim"
    use "wsdjeg/luarefvim"

    -- use { "rcarriga/nvim-dap-ui", requires = { "mfussenegger/nvim-dap" } }
    -- use "mfussenegger/nvim-dap-python"


    use {"stevearc/dressing.nvim"}

    use "wfxr/minimap.vim"
    use {
    'AckslD/nvim-revJ.lua',
    requires = {'kana/vim-textobj-user', 'sgur/vim-textobj-parameter'},
    -- or
    -- requires = {'wellle/targets.vim'},
    -- or ...
}

end)

